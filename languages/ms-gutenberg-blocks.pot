msgid ""
msgstr ""
"Content-Type: text/plain; charset=utf-8\n"
"X-Generator: babel-plugin-makepot\n"

#: src/blocks/anchor/edit.js:25
msgid "Choose a nice ID for your anchor. Should be a URL friendly slug name."
msgstr ""

#: src/blocks/anchor/edit.js:38
#: src/blocks/anchor/index.js:38
msgid "Anchor"
msgstr ""

#: src/blocks/animate/edit.js:44
msgid "Animations"
msgstr ""

#: src/blocks/animate/edit.js:46
#: src/blocks/animate/index.js:38
msgid "Animation"
msgstr ""

#: src/blocks/animate/edit.js:52
msgid "Delay (ms)"
msgstr ""

#: src/blocks/animate/edit.js:60
msgid "Duration (ms)"
msgstr ""

#: src/blocks/animate/edit.js:68
msgid "Offset (px)"
msgstr ""

#: src/blocks/animate/edit.js:69
msgid "Offset (in pixels) from the original trigger point."
msgstr ""

#: src/blocks/animate/edit.js:75
msgid "Easing"
msgstr ""

#: src/blocks/animate/edit.js:76
msgid "Easing function for animations."
msgstr ""

#: src/blocks/animate/edit.js:82
msgid "Once"
msgstr ""

#: src/blocks/animate/edit.js:83
msgid "Whether the animation should happen only once while scrolling down."
msgstr ""

#: src/blocks/animate/edit.js:88
msgid "Mirror"
msgstr ""

#: src/blocks/animate/edit.js:89
msgid "Whether elements should animate out while scrolling past them."
msgstr ""

#: src/blocks/animate/edit.js:94
msgid "Anchor placement"
msgstr ""

#: src/blocks/animate/edit.js:95
msgid ""
"Defines which position of the element regarding to the browser window "
"should trigger the animation."
msgstr ""

#: src/blocks/animate/index.js:35
msgid "Animate blocks inside this container."
msgstr ""

#: src/blocks/animate/index.js:37
msgid "Animate"
msgstr ""

#: src/blocks/animate/options/animation-options.js:115
msgid "No animation"
msgstr ""

#: src/blocks/container/edit.js:100
msgid "Content width"
msgstr ""

#: src/blocks/container/edit.js:102
msgid "Small"
msgstr ""

#: src/blocks/container/edit.js:103
msgid "Normal"
msgstr ""

#: src/blocks/container/edit.js:104
msgid "Wide"
msgstr ""

#: src/blocks/container/edit.js:105
msgid "Full"
msgstr ""

#: src/blocks/container/edit.js:114
msgid "Background settings"
msgstr ""

#: src/blocks/container/edit.js:118
msgid "Background color type"
msgstr ""

#: src/blocks/container/edit.js:120
msgid "Image size"
msgstr ""

#: src/blocks/container/edit.js:130
msgid "Single"
msgstr ""

#: src/blocks/container/edit.js:141
msgid "Gradient"
msgstr ""

#: src/blocks/container/edit.js:148
msgid "Background color #1"
msgstr ""

#: src/blocks/container/edit.js:149
msgid "Background color"
msgstr ""

#: src/blocks/container/edit.js:160
msgid "Background color #2"
msgstr ""

#: src/blocks/container/edit.js:171
msgid "Gradient direction (degrees)"
msgstr ""

#: src/blocks/container/edit.js:184
msgid "Opacity"
msgstr ""

#: src/blocks/container/edit.js:194
msgid "Background media"
msgstr ""

#: src/blocks/container/edit.js:259
msgid "Background image preview"
msgstr ""

#: src/blocks/container/edit.js:292
msgid "Focal Point Picker"
msgstr ""

#: src/blocks/container/edit.js:307
msgid "Fixed background image"
msgstr ""

#: src/blocks/container/edit.js:319
msgid "Parallax"
msgstr ""

#: src/blocks/container/edit.js:332
msgid "Parallax speed"
msgstr ""

#: src/blocks/container/edit.js:333
msgid ""
"The speed at which the parallax effect runs. 0 means the image will appear "
"fixed in place, and 10 means the image will flow at the same speed as the "
"page content."
msgstr ""

#: src/blocks/container/edit.js:350
msgid "Dark background"
msgstr ""

#: src/blocks/container/edit.js:351
msgid ""
"Adds an extra class to the container which helps in styling the text to be "
"more readable."
msgstr ""

#: src/blocks/container/edit.js:361
msgid "Margin & Padding"
msgstr ""

#: src/blocks/container/edit.js:364
#: src/blocks/panel/index.js:111
msgid "Padding (rem)"
msgstr ""

#: src/blocks/container/edit.js:388
#: src/blocks/panel/index.js:135
msgid "Left"
msgstr ""

#: src/blocks/container/edit.js:399
#: src/blocks/panel/index.js:146
msgid "Right"
msgstr ""

#: src/blocks/container/edit.js:409
#: src/blocks/responsive-accordion-tabs/edit.js:443
msgid "Margin (rem)"
msgstr ""

#: src/blocks/container/edit.js:411
#: src/blocks/panel/index.js:113
#: src/blocks/responsive-accordion-tabs/edit.js:445
msgid "Top"
msgstr ""

#: src/blocks/container/edit.js:422
#: src/blocks/panel/index.js:124
#: src/blocks/responsive-accordion-tabs/edit.js:456
msgid "Bottom"
msgstr ""

#: src/blocks/container/edit.js:432
#: src/blocks/panel/index.js:156
#: src/blocks/responsive-accordion-tabs/edit.js:466
msgid "Scaling on tablet and mobile"
msgstr ""

#: src/blocks/container/edit.js:435
#: src/blocks/panel/index.js:159
#: src/blocks/responsive-accordion-tabs/edit.js:469
msgid "Percentage based on the values selected in margin and padding."
msgstr ""

#: src/blocks/container/edit.js:442
#: src/blocks/panel/index.js:166
#: src/blocks/responsive-accordion-tabs/edit.js:476
msgid "Tablet scaling factor (%)"
msgstr ""

#: src/blocks/container/edit.js:454
#: src/blocks/panel/index.js:178
#: src/blocks/responsive-accordion-tabs/edit.js:488
msgid "Mobile scaling factor (%)"
msgstr ""

#: src/blocks/container/edit.js:78
msgid "Size settings"
msgstr ""

#: src/blocks/container/edit.js:82
msgid "Height"
msgstr ""

#: src/blocks/container/edit.js:86
msgid "Half-screen height"
msgstr ""

#: src/blocks/container/edit.js:90
msgid "Full-screen height"
msgstr ""

#: src/blocks/container/index.js:39
msgid "Container"
msgstr ""

#: src/blocks/layouts/edit.js:104
msgid "Add Layout"
msgstr ""

#: src/blocks/layouts/edit.js:117
msgid "Layout Selector"
msgstr ""

#: src/blocks/layouts/edit.js:118
msgid "Launch the layout library to browse pre-designed sections."
msgstr ""

#: src/blocks/layouts/edit.js:125
msgid "Choose a layout"
msgstr ""

#: src/blocks/layouts/edit.js:138
msgid ""
"There are no layouts available! Create some at the layouts post type. Once "
"you have created a new layout, refresh this page to load it here."
msgstr ""

#: src/blocks/layouts/edit.js:140
msgid "Go to layouts"
msgstr ""

#: src/blocks/layouts/edit.js:145
msgid "Remove this block"
msgstr ""

#: src/blocks/layouts/edit.js:155
msgid "Layout Categories"
msgstr ""

#: src/blocks/layouts/edit.js:166
msgid "Amount of available layouts:"
msgstr ""

#: src/blocks/layouts/index.js:54
msgid "Layouts"
msgstr ""

#: src/blocks/layouts/index.js:54
msgid "Insert Layout"
msgstr ""

#: src/blocks/panel/index.js:108
msgid "Padding"
msgstr ""

#: src/blocks/panel/index.js:46
msgid "Panel"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:319
#: src/blocks/responsive-accordion-tabs/index.js:45
msgid "Responsive Accordion Tabs"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:323
msgid "Desktop"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:349
msgid "Tablet"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:375
msgid "Mobile"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:385
msgid "Accordion"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:396
msgid "Tabs"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:407
msgid "Accordion settings"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:411
msgid "Initially close all panes"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:420
msgid "Allow all panes closed"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:429
msgid "Allow multiple panes opened"
msgstr ""

#: src/blocks/responsive-accordion-tabs/edit.js:440
msgid "Margin"
msgstr ""

#: src/components/color-palette-control/index.js:16
#. first %s: The type of color (e.g. background color), second %s: the color name or value (e.g. red or #ff0000)
msgid "(current %s: %s)"
msgstr ""

#: src/formats/no-translate/index.js:38
msgid "No translate"
msgstr ""

#: src/options/conditional-logic/attributes.js:62
msgid "Conditional logic"
msgstr ""

#: src/options/conditional-logic/attributes.js:67
msgid "Hide block"
msgstr ""

#: src/options/conditional-logic/attributes.js:68
msgid ""
"Hide this block based on a valid PHP conditional (for example: has_tag()) "
"that equals to true. Only use this if you know what you are doing!"
msgstr ""