# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [5.2.2] - 2022-05-18
### Fixed
- Background video in container now scales properly on Internet Explorer 11.

## [5.2.1] - 2022-05-16
### Fixed
- Production builds.

## [5.2.0] - 2022-05-16

### Changed
- Updated container block to version 2 of the block API.

### Fixed
- Fixed background color styling not appearing in editor.
- Fixed .has-background styling from overflowing into the content.
- Fixed not being able to insert video as background.
- Fixed spacings not being applied to containers in the editor.

## [5.1.1] - 2021-02-25

### Changed
- Removed registering layouts post type (layouts) and taxonomy (layouts_category). Please register them in your own theme.

### Fixed
- Fixed replacing layouts block with selected layout not working.

---

## [5.1.0] - 2021-02-22

### Changed
- Refresh all icons.

---

## [5.0.2] - 2021-02-19

### Changed
- Refresh layouts block and button icon.

- ### Fixed
- Fixed missing layout button in toolbar.

---

## [5.0.1] - 2021-02-17

### Changed
- Always open animation settings panel.

- ### Fixed
- Fixed missing translations.

---

## [5.0.0] - 2021-02-17

### Added
- Added a new animate block for animating content with Animate On Scroll Library.

---

## [4.6.1] - 2021-02-01

### Changed
- 'Initially close all panes' now only works when all breakpoints are set to accordion, fixing an issue where content of the first opened tab was duplicated once when clicking on another.

---

## [4.6.0] - 2021-02-01

### Added
- Added a toggle to the container which helps developers style content on a dark background.

### Changed
- Converted the container block from a class component to a functional component, improving the responsiveness of the block.
- Renamed most classes applied to the root container div while keeping backwards compatibility.
- Improved unique ID generation on the container block, which should reduce styles mismatch.

### Fixed
- Fixed blocks overflowing when a container is half height or full height, blocking the user from adding more blocks after a container.

---

## [4.5.2] - 2020-10-24

### Changed
- Only add conditional logic to core, ACF, and our Gutenberg blocks, in order to prevent invalid attributes error.

---

## [4.5.1] - 2020-10-24

### Fixed
- Fixed missing translations.

---

## [4.5.0] - 2020-10-24

### Added
- Added a new anchor block for quick navigating to a section on a page.

---

## [4.4.3] - 2020-10-21

### Changed
- Restore eval() function because it is needed for evaluating conditionals.

---

## [4.4.2] - 2020-10-21

### Changed
- Reverse visibility logic to be more clear on how to use it.
- Remove unsafe eval() function from visibility logic.

---

## [4.4.1] - 2020-10-21

### Fixed
- Prevent block corruption when the new setting is not defined on a block.
- Fixed missing translations.

---

## [4.4.0] - 2020-10-21

### Added
- Added a block visibility setting to all Gutenberg blocks where blocks can be hidden by supplying a PHP conditional that equals true.

---

## [4.3.1] - 2020-09-28

### Fixed
- Fixed a deprecation warning.

---

## [4.3.0] - 2020-09-28

### Added
- Added a new format for selecting words that should not be translated by Weglot.

---

## [4.2.2] - 2020-05-25

### Changed
- Added a button appender to panels for clarity.

### Fixed
- Fixed an issue where adding new tabs in the responsive accordion tabs block created no panel for inserting content.
- Fixed an issue where removing tabs would select the first tab, but not the first panel, preventing content from showing.
- Fixed a deprecation warning with allowed formats for tab titles in the console.

---

## [4.2.1] - 2020-05-25

### Changed
- Fixed deprecation warning with button styling in the console.

### Fixed
- Fixed an issue where removing tabs removed the wrong tab content.

---

## [4.2.0] - 2020-02-24

### Added
- Added the option to enable parallax in container block.

---

## [4.1.2] - 2020-02-14

### Fixed
- Fixed an issue where admin styles were loading in the frontend, causing all sorts of issues with Gutenberg 7.5.0.

---

## [4.1.1] - 2020-01-29

### Fixed
- Fixed an issue where existing responsive accordion tabs blocks would generate a JSON error on save.

---

## [4.1.0] - 2020-01-29

### Added
- Added the option to initially close all panes in responsive accordion tabs block.

---

## [4.0.0] - 2019-11-27

### Added
- Added new layouts block to insert a premade arrangement of blocks into the current page.

---

## [3.1.0] - 2019-11-27

### Added
- Added support for widest alignment when transforming blocks into a container block.
- Added scaled spacings for mobile and tablet margin and padding.

### Fixed
- Fixed an issue when a duplicated container did not contain a unique ID, which caused both blocks to synchronize (changed) attributes.

---

## [3.0.1] - 2019-10-03

### Fixed
- Set the background video css to correctly apply only on video backgrounds.

---

## [3.0.0] - 2019-06-27

### Added
- Added a responsive accordion tabs block.

---

## [2.4.1] - 2019-06-18

### Changed
- Gutenberg 5.9.0 compatibility.

---

## [2.4.0] - 2019-06-13

### Added
- Added an experimental option to group multiple blocks in a container block. Does not work (yet) with ACF blocks.

---

## [2.3.1] - 2019-06-06

### Fixed
- Fixed possible overflow issue with video backgrounds.

---

## [2.3.0] - 2019-05-24

### Added
- Added Dutch translations.

---

## [2.2.0] - 2019-05-16

### Changed
- Make it easier to select the first block inside the container.

---

## [2.1.1] - 2019-05-16

### Fixed
- Fixed wrong version number in the plugin.

---

## [2.1.0] - 2019-05-16

### Added
- Added a 'small' option to the content width.

---

## [2.0.1] - 2019-04-19

### Fixed
- Prevent old container blocks from breaking when disabling fixed background.

---

## [2.0.0] - 2019-04-19
This is a big update with breaking changes. You probably need to recreate your container blocks.

### Added
- Renamed the plugin in preparation of supporting multiple blocks in the near future.
- Added support for gradient backgrounds.
- Added support for video backgrounds.
- Added a focal point picker when using a background image.

### Changed
- Changed the icon once again for a clearer one.

### Removed
- Removed the background type select; it is now dynamically defined, based on the selected options.

### Internal
- Added preparations for translations.

---

## [1.5.4] - 2019-03-28
### Changed
- Changed the icon and icon color to match other custom MediaSoep blocks.

### Fixed
- Added missing build files.

---

## [1.5.3] - 2019-03-28
### Removed
- Removed the default template blocks in the container as they would reappear if the container was empty when the page was being saved.

---

## [1.5.2] - 2019-03-27
### Fixed
- Added missing build files.

---

## [1.5.1] - 2019-03-27
### Changed
- New minimum and maximum sizes for margins and paddings.

---

## [1.5.0] - 2019-02-19
### Added
- Added options to choose height and content width (based on alignments).

---

## [1.4.0] - 2019-02-12
### Added
- Added the option to select a color background or an image background.

### Fixed
- Each container now has their own ID to correctly apply styling.

---

## [1.3.0] - 2019-02-07
### Added
- Added support for setting paddings and margins.

---

## [1.2.0] - 2019-02-07
### Added
- Added support for setting a background color.

---

## [1.1.1] - 2019-02-06
### Added
- A changelog file.

### Fixed
- Version number not corresponding with packagist/git version.

---

## [1.1.0] - 2019-02-06
### Added
- Support for alignwide and alignfull.

### Changed
- Use the formatting of create-guten-block block.js.

---

## [1.0.0] - 2019-02-06
### Added
- Initial project structure.
