<?php
/**
 * Plugin Name: MS Gutenberg Blocks
 * Plugin URI: https://www.mediasoep.nl/
 * Description: MS Gutenberg Blocks is a Gutenberg plugin consisting of various blocks created via create-guten-block.
 * Author: MediaSoep
 * Author URI: https://www.mediasoep.nl/
 * Requires at least: 5.6
 * Version: 5.2.2
 *
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/blocks.php';
