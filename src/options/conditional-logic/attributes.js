/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { addFilter } = wp.hooks;
const { Fragment } = wp.element;
const { createHigherOrderComponent } = wp.compose;
const { InspectorControls } = wp.blockEditor;
const { PanelBody, TextareaControl } = wp.components;

/**
 * Extend all blocks with new attributes.
 *
 * @param {Object} settings Original block settings.
 *
 * @return {Object} Filtered block settings.
 */
function addAttributes( settings ) {
	if ( ! /^(ms\/|core\/|acf\/)/.test( settings.name ) ) {
		return settings;
	}

	if ( typeof settings.attributes !== 'undefined' && typeof settings.attributes.mediasoep === 'undefined' ) {
		settings.attributes = {
			...settings.attributes,
			mediasoep: {
				type: 'object',
				default: {
					visibilityLogic: '',
				},
			},
		};
	}

	return settings;
}

/**
 * Add custom MediaSoep attributes to blocks.
 *
 * @param {Function} BlockEdit Original component.
 * @return {string} Wrapped component.
 */
const withAttributes = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {
		const { attributes, setAttributes } = props;
		const { mediasoep } = attributes;

		if ( mediasoep === undefined ) {
			return (
				<Fragment>
					<BlockEdit { ...props } />
				</Fragment>
			);
		}

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'Conditional logic', 'ms-gutenberg-blocks' ) }
						initialOpen={ false }
					>
						<TextareaControl
							rows={ 2 }
							label={ __( 'Hide block', 'ms-gutenberg-blocks' ) }
							help={ __(
								'Hide this block based on a valid PHP conditional (for example: has_tag()) that equals to true. Only use this if you know what you are doing!',
								'ms-gutenberg-blocks'
							) }
							value={ mediasoep.visibilityLogic ? mediasoep.visibilityLogic : '' }
							onChange={ ( newValue ) => {
								delete mediasoep.visibilityLogic;
								const withVisibilityLogic = Object.assign(
									{ visibilityLogic: newValue },
									mediasoep
								);
								setAttributes( { mediasoep: withVisibilityLogic } );
							} }
						/>
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withAttributes' );

addFilter(
	'blocks.registerBlockType',
	'mediasoep/custom/attributes',
	addAttributes
);

addFilter( 'editor.BlockEdit', 'mediasoep/attributes', withAttributes );
