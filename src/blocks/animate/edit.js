/**
 * BLOCK: MS Gutenberg Blocks - Animate
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Fragment } = wp.element;
const {
	PanelBody,
	RangeControl,
	SelectControl,
	TextControl,
	ToggleControl,
} = wp.components;
const {
	InnerBlocks,
	InspectorControls,
} = wp.blockEditor;

/**
 * Internal dependencies
 */
import anchorPlacementOptions from './options/anchor-placement-options';
import animationOptions from './options/animation-options';
import easingOptions from './options/easing-options';

const edit = ( { attributes, setAttributes, className } ) => {
	const {
		animation = '',
		animationOffset,
		animationDelay,
		animationDuration,
		animationEasing,
		animationOnce,
		animationMirror,
		animationAnchorPlacement,
	} = attributes;

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody title={ __( 'Animations', 'ms-gutenberg-blocks' ) } initialOpen={ true }>
					<SelectControl
						label={ __( 'Animation', 'ms-gutenberg-blocks' ) }
						value={ animation }
						options={ animationOptions }
						onChange={ ( selectedOption ) => setAttributes( { animation: selectedOption } ) }
					/>
					<RangeControl
						label={ __( 'Delay (ms)', 'ms-gutenberg-blocks' ) }
						value={ animationDelay }
						onChange={ ( value ) => setAttributes( { animationDelay: value } ) }
						min={ 0 }
						max={ 3000 }
						step={ 50 }
					/>
					<RangeControl
						label={ __( 'Duration (ms)', 'ms-gutenberg-blocks' ) }
						value={ animationDuration }
						onChange={ ( value ) => setAttributes( { animationDuration: value } ) }
						min={ 0 }
						max={ 3000 }
						step={ 50 }
					/>
					<TextControl
						label={ __( 'Offset (px)', 'ms-gutenberg-blocks' ) }
						help={ __( 'Offset (in pixels) from the original trigger point.', 'ms-gutenberg-blocks' ) }
						type="number"
						value={ animationOffset }
						onChange={ ( value ) => setAttributes( { animationOffset: value } ) }
					/>
					<SelectControl
						label={ __( 'Easing', 'ms-gutenberg-blocks' ) }
						help={ __( 'Easing function for animations.', 'ms-gutenberg-blocks' ) }
						value={ animationEasing }
						options={ easingOptions }
						onChange={ ( selectedOption ) => setAttributes( { animationEasing: selectedOption } ) }
					/>
					<ToggleControl
						label={ __( 'Once', 'ms-gutenberg-blocks' ) }
						help={ __( 'Whether the animation should happen only once while scrolling down.', 'ms-gutenberg-blocks' ) }
						checked={ animationOnce }
						onChange={ ( checked ) => setAttributes( { animationOnce: checked } ) }
					/>
					<ToggleControl
						label={ __( 'Mirror', 'ms-gutenberg-blocks' ) }
						help={ __( 'Whether elements should animate out while scrolling past them.', 'ms-gutenberg-blocks' ) }
						checked={ animationMirror }
						onChange={ ( checked ) => setAttributes( { animationMirror: checked } ) }
					/>
					<SelectControl
						label={ __( 'Anchor placement', 'ms-gutenberg-blocks' ) }
						help={ __( 'Defines which position of the element regarding to the browser window should trigger the animation.', 'ms-gutenberg-blocks' ) }
						value={ animationAnchorPlacement }
						options={ anchorPlacementOptions }
						onChange={ ( selectedOption ) => setAttributes( { animationAnchorPlacement: selectedOption } ) }
					/>
				</PanelBody>
			</InspectorControls>
			<div className={ className }>
				<InnerBlocks renderAppender={ InnerBlocks.ButtonBlockAppender } />
			</div>
		</Fragment>
	);
};

export default edit;
