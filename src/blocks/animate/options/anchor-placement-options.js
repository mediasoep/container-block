const { applyFilters } = wp.hooks;
const { __ } = wp.i18n;

let anchorPlacementOptions = [
	{
		label: 'Top Bottom',
		value: 'top-bottom',
	},
	{
		label: 'Top Center',
		value: 'top-center',
	},
	{
		label: 'Top Top',
		value: 'top-top',
	},
	{
		label: 'Center Bottom',
		value: 'center-bottom',
	},
	{
		label: 'Center Center',
		value: 'center-center',
	},
	{
		label: 'Center Top',
		value: 'center-top',
	},
	{
		label: 'Bottom Bottom',
		value: 'bottom-bottom',
	},
	{
		label: 'Bottom Center',
		value: 'bottom-center',
	},
	{
		label: 'Bottom Top',
		value: 'bottom-top',
	},
];
anchorPlacementOptions = applyFilters( 'animateBlocks.anchorPlacementOptions', anchorPlacementOptions );

export default anchorPlacementOptions;
