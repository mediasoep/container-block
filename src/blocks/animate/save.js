/**
 * BLOCK: MS Gutenberg Blocks - Animate
 */

/**
 * WordPress dependencies
 */
const { InnerBlocks } = wp.blockEditor;

const aosDefaultOptions = {
	animation: 'fade',
	offset: 120,
	delay: 0,
	duration: 400,
	easing: 'ease',
	once: true,
	mirror: false,
	anchorPlacement: 'top-bottom',
};

const save = ( { attributes, className } ) => {
	const {
		animation = '',
		animationOffset,
		animationDelay,
		animationDuration,
		animationEasing,
		animationOnce,
		animationMirror,
		animationAnchorPlacement,
	} = attributes;

	return (
		<div
			className={ className }
			data-aos={ animation }
			{ ...( animationDelay !== aosDefaultOptions.delay && { 'data-aos-delay': animationDelay } ) }
			{ ...( animationDuration !== aosDefaultOptions.duration && { 'data-aos-duration': animationDuration } ) }
			{ ...( animationOffset !== aosDefaultOptions.offset && { 'data-aos-offset': animationOffset } ) }
			{ ...( animationEasing !== aosDefaultOptions.easing && { 'data-aos-easing': animationEasing } ) }
			{ ...( animationOnce !== aosDefaultOptions.once && { 'data-aos-once': animationOnce } ) }
			{ ...( animationMirror !== aosDefaultOptions.mirror && { 'data-aos-mirror': animationMirror } ) }
			{ ...( animationAnchorPlacement !== aosDefaultOptions.anchorPlacement && { 'data-aos-anchor-placement': animationAnchorPlacement } ) }
		>
			<InnerBlocks.Content />
		</div>
	);
};

export default save;
