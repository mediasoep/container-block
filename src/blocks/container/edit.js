/**
 * BLOCK: MS Gutenberg Blocks - Container
 */

/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { Fragment } = wp.element;
const {
	BaseControl,
	Button,
	ButtonGroup,
	Dashicon,
	FocalPointPicker,
	PanelBody,
	RangeControl,
	SelectControl,
	ToggleControl,
} = wp.components;
const {
	InnerBlocks,
	InspectorControls,
	MediaUpload,
	MediaUploadCheck,
	useBlockProps,
} = wp.blockEditor;

/**
 * Internal dependencies
 */
import getAppearance from './appearance.js';
import ColorPaletteControl from './../../components/color-palette-control/index.js';
import withUniqueId from './../../utils/withUniqueId.js';

const edit = ( props ) => {
	const {
		className,
		setAttributes,
		attributes: {
			uniqueID,
			height,
			contentWidth,
			backgroundType,
			backgroundOpacity,
			backgroundColor,
			backgroundColor2,
			backgroundColorDirection = 0,
			backgroundColorType,
			backgroundImageID,
			backgroundImageUrl,
			backgroundFocalPoint,
			backgroundImageAttachment,
			backgroundImageParallax,
			backgroundImageParallaxSpeed,
			isDark,
			topMargin,
			bottomMargin,
			topPadding,
			bottomPadding,
			leftPadding,
			rightPadding,
			tabletScalingFactor = 100,
			mobileScalingFactor = 50,
		},
	} = props;

	const { classes, wrapperClasses, inlineStyles, internalEditStyles } =
		getAppearance( props );

	return (
		<Fragment>
			<InspectorControls>
				<PanelBody
					title={ __( 'Size settings', 'ms-gutenberg-blocks' ) }
					initialOpen={ true }
				>
					<SelectControl
						label={ __( 'Height', 'ms-gutenberg-blocks' ) }
						options={ [
							{ label: __( 'Normal', 'ms-gutenberg-blocks' ), value: 'normal' },
							{
								label: __( 'Half-screen height', 'ms-gutenberg-blocks' ),
								value: 'half',
							},
							{
								label: __( 'Full-screen height', 'ms-gutenberg-blocks' ),
								value: 'full',
							},
						] }
						value={ height }
						onChange={ ( value ) => {
							setAttributes( { height: value } );
						} }
					/>
					<SelectControl
						label={ __( 'Content width', 'ms-gutenberg-blocks' ) }
						options={ [
							{ label: __( 'Small', 'ms-gutenberg-blocks' ), value: 'small' },
							{ label: __( 'Normal', 'ms-gutenberg-blocks' ), value: 'normal' },
							{ label: __( 'Wide', 'ms-gutenberg-blocks' ), value: 'wide' },
							{ label: __( 'Full', 'ms-gutenberg-blocks' ), value: 'full' },
						] }
						value={ contentWidth }
						onChange={ ( value ) => {
							setAttributes( { contentWidth: value } );
						} }
					/>
				</PanelBody>
				<PanelBody
					title={ __( 'Background settings', 'ms-gutenberg-blocks' ) }
					initialOpen={ true }
				>
					<BaseControl
						label={ __( 'Background color type', 'ms-gutenberg-blocks' ) }
					>
						<ButtonGroup aria-label={ __( 'Image size', 'ms-gutenberg-blocks' ) }>
							<Button
								isPrimary={ backgroundColorType === '' }
								isSecondary={ backgroundColorType !== '' }
								onClick={ () => {
									setAttributes( {
										backgroundColorType: '',
									} );
								} }
							>
								{ __( 'Single', 'ms-gutenberg-blocks' ) }
							</Button>
							<Button
								isPrimary={ backgroundColorType === 'gradient' }
								isSecondary={ backgroundColorType !== 'gradient' }
								onClick={ () => {
									setAttributes( {
										backgroundColorType: 'gradient',
									} );
								} }
							>
								{ __( 'Gradient', 'ms-gutenberg-blocks' ) }
							</Button>
						</ButtonGroup>
					</BaseControl>
					<ColorPaletteControl
						label={
							backgroundColorType === 'gradient' ?
								__( 'Background color #1', 'ms-gutenberg-blocks' ) :
								__( 'Background color', 'ms-gutenberg-blocks' )
						}
						value={ backgroundColor }
						onChange={ ( value ) => {
							setAttributes( {
								backgroundColor: value,
							} );
						} }
					/>
					{ backgroundColorType === 'gradient' && (
						<ColorPaletteControl
							label={ __( 'Background color #2', 'ms-gutenberg-blocks' ) }
							value={ backgroundColor2 }
							onChange={ ( value ) => {
								setAttributes( {
									backgroundColor2: value,
								} );
							} }
						/>
					) }
					{ backgroundColorType === 'gradient' && (
						<RangeControl
							label={ __( 'Gradient direction (degrees)', 'ms-gutenberg-blocks' ) }
							value={ backgroundColorDirection }
							onChange={ ( value ) => {
								setAttributes( {
									backgroundColorDirection: value,
								} );
							} }
							min={ 0 }
							max={ 360 }
							step={ 10 }
						/>
					) }
					<RangeControl
						label={ __( 'Opacity', 'ms-gutenberg-blocks' ) }
						value={ backgroundOpacity }
						onChange={ ( value ) => {
							setAttributes( {
								backgroundOpacity: value,
							} );
						} }
						min={ 0 }
						max={ 10 }
					/>
					<BaseControl label={ __( 'Background media', 'ms-gutenberg-blocks' ) }>
						<MediaUploadCheck>
							<MediaUpload
								allowedTypes={ [ 'image', 'video' ] }
								onSelect={ ( media ) => {
									const isFullSizeDefined = media.sizes !== undefined && media.sizes.full !== undefined;
									setAttributes( {
										backgroundImageUrl: media.url,
										backgroundImageID: media.id,
										backgroundImageHeight: isFullSizeDefined ? media.sizes.full.height : null,
										backgroundImageWidth: isFullSizeDefined ? media.sizes.full.width : null,
										backgroundType: media.type,
									} )
								} }
								value={ backgroundImageID }
								render={ ( obj ) => {
									return (
										<Fragment>
											{ backgroundImageUrl && (
												<div className="ms-image-preview-wrapper">
													<button
														className="ms-image-preview-remove"
														onClick={ () => {
															setAttributes( {
																backgroundImageUrl: '',
																backgroundImageID: 0,
																backgroundType: '',
															} );
														} }
													>
														<Dashicon icon="no" />
													</button>
													{ backgroundType === 'video' && (
														<video
															className="ms-video-preview"
															autoPlay
															muted
															loop
															src={ backgroundImageUrl }
															onClick={ obj.open }
															onKeyDown={ ( event ) => {
																if ( event.keyCode === 13 ) {
																	obj.open();
																}
															} }
														/>
													) }
													{ backgroundType === 'image' && (
														/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions */
														<img
															className="ms-image-preview"
															src={ backgroundImageUrl }
															onClick={ obj.open }
															onKeyDown={ ( event ) => {
																if ( event.keyCode === 13 ) {
																	obj.open();
																}
															} }
															alt={ __(
																'Background image preview',
																'ms-gutenberg-blocks'
															) }
														/>
													) }
												</div>
											) }
											{ ! backgroundImageUrl && (
												<div
													className="ms-placeholder"
													onClick={ obj.open }
													onKeyDown={ ( event ) => {
														if ( event.keyCode === 13 ) {
															obj.open();
														}
													} }
													role="button"
													tabIndex={ 0 }
												>
													<Dashicon icon="format-image" />
												</div>
											) }
										</Fragment>
									);
								} }
							/>
						</MediaUploadCheck>
					</BaseControl>
					{ backgroundType === 'image' &&
						backgroundImageUrl &&
						! backgroundImageAttachment && (
						<FocalPointPicker
							label={ __( 'Focal Point Picker', 'ms-gutenberg-blocks' ) }
							url={ backgroundImageUrl }
							value={ backgroundFocalPoint }
							onChange={ ( value ) =>
								setAttributes( {
									backgroundFocalPoint: value,
								} )
							}
						/>
					) }
					{ backgroundType === 'image' &&
						backgroundImageUrl &&
						!! backgroundImageUrl.length && (
						<Fragment>
							<ToggleControl
								label={ __( 'Fixed background image', 'ms-gutenberg-blocks' ) }
								checked={ backgroundImageAttachment }
								onChange={ () => {
									setAttributes( {
										backgroundImageAttachment: ! backgroundImageAttachment,
									} );
									setAttributes( {
										backgroundImageParallax: false,
									} );
								} }
							/>
							<ToggleControl
								label={ __( 'Parallax', 'ms-gutenberg-blocks' ) }
								checked={ backgroundImageParallax }
								onChange={ () => {
									setAttributes( {
										backgroundImageParallax: ! backgroundImageParallax,
									} );
									setAttributes( {
										backgroundImageAttachment: false,
									} );
								} }
							/>
							{ backgroundImageParallax && (
								<RangeControl
									label={ __( 'Parallax speed', 'ms-gutenberg-blocks' ) }
									help={ __(
										'The speed at which the parallax effect runs. 0 means the image will appear fixed in place, and 10 means the image will flow at the same speed as the page content.',
										'ms-gutenberg-blocks'
									) }
									value={ backgroundImageParallaxSpeed }
									onChange={ ( value ) => {
										setAttributes( {
											backgroundImageParallaxSpeed: value,
										} );
									} }
									min={ 0 }
									max={ 10 }
								/>
							) }
						</Fragment>
					) }
					<ToggleControl
						label={ __( 'Dark background', 'ms-gutenberg-blocks' ) }
						help={ __(
							'Adds an extra class to the container which helps in styling the text to be more readable.',
							'ms-gutenberg-blocks'
						) }
						checked={ isDark }
						onChange={ () => {
							setAttributes( {
								isDark: ! isDark,
							} );
						} }
					/>
				</PanelBody>
				<PanelBody
					title={ __( 'Margin & Padding', 'ms-gutenberg-blocks' ) }
					initialOpen={ true }
				>
					<h2>{ __( 'Padding (rem)', 'ms-gutenberg-blocks' ) }</h2>
					<RangeControl
						label={ __( 'Top', 'ms-gutenberg-blocks' ) }
						value={ topPadding }
						onChange={ ( value ) => {
							setAttributes( {
								topPadding: value,
							} );
						} }
						min={ 0 }
						max={ 20 }
					/>
					<RangeControl
						label={ __( 'Bottom', 'ms-gutenberg-blocks' ) }
						value={ bottomPadding }
						onChange={ ( value ) => {
							setAttributes( {
								bottomPadding: value,
							} );
						} }
						min={ 0 }
						max={ 20 }
					/>
					<RangeControl
						label={ __( 'Left', 'ms-gutenberg-blocks' ) }
						value={ leftPadding }
						onChange={ ( value ) => {
							setAttributes( {
								leftPadding: value,
							} );
						} }
						min={ 0 }
						max={ 20 }
					/>
					<RangeControl
						label={ __( 'Right', 'ms-gutenberg-blocks' ) }
						value={ rightPadding }
						onChange={ ( value ) => {
							setAttributes( {
								rightPadding: value,
							} );
						} }
						min={ 0 }
						max={ 20 }
					/>
					<h2>{ __( 'Margin (rem)', 'ms-gutenberg-blocks' ) }</h2>
					<RangeControl
						label={ __( 'Top', 'ms-gutenberg-blocks' ) }
						value={ topMargin }
						onChange={ ( value ) => {
							setAttributes( {
								topMargin: value,
							} );
						} }
						min={ -20 }
						max={ 20 }
					/>
					<RangeControl
						label={ __( 'Bottom', 'ms-gutenberg-blocks' ) }
						value={ bottomMargin }
						onChange={ ( value ) => {
							setAttributes( {
								bottomMargin: value,
							} );
						} }
						min={ -20 }
						max={ 20 }
					/>
					<h2>{ __( 'Scaling on tablet and mobile', 'ms-gutenberg-blocks' ) }</h2>
					<p>
						<i>
							{ __(
								'Percentage based on the values selected in margin and padding.',
								'ms-gutenberg-blocks'
							) }
						</i>
					</p>
					<RangeControl
						label={ __( 'Tablet scaling factor (%)', 'ms-gutenberg-blocks' ) }
						value={ tabletScalingFactor }
						onChange={ ( value ) => {
							setAttributes( {
								tabletScalingFactor: value,
							} );
						} }
						min={ 5 }
						max={ 100 }
						step={ 5 }
					/>
					<RangeControl
						label={ __( 'Mobile scaling factor (%)', 'ms-gutenberg-blocks' ) }
						value={ mobileScalingFactor }
						onChange={ ( value ) => {
							setAttributes( {
								mobileScalingFactor: value,
							} );
						} }
						min={ 5 }
						max={ 100 }
						step={ 5 }
					/>
				</PanelBody>
			</InspectorControls>
			<div
				{ ...useBlockProps( {
					id: `ms-container-${ uniqueID }`,
					className: classnames( className, classes ),
					style: inlineStyles,
				} ) }
			>
				{ backgroundType === 'video' && (
					<video
						className="ms-video-background"
						autoPlay
						muted
						loop
						src={ backgroundImageUrl }
					/>
				) }
				<div className={ wrapperClasses }>
					<InnerBlocks renderAppender={ InnerBlocks.ButtonBlockAppender } />
				</div>
				{ internalEditStyles && (
					<style>
						{ internalEditStyles }
					</style>
				) }
			</div>
		</Fragment>
	);
};

export default withUniqueId( edit );
