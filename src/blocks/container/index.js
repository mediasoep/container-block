/**
 * BLOCK: MS Gutenberg Blocks - Container
 */

/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
const { registerBlockType } = wp.blocks;
const { Path, SVG } = wp.components;

/**
 * Internal dependencies
 */
import attributes from './attributes';
import edit from './edit';
import save from './save';
import transforms from './transforms';
import deprecated from './deprecated';

//  Import CSS.
import './style.scss';
import './editor.scss';

/**
 * Register: a Gutenberg Block.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'ms/container', {
	apiVersion: 2,
	title: __( 'Container', 'ms-gutenberg-blocks' ),
	icon: ( <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" width="24" height="24">
		<Path fill="#f29f95" d="M148 64H24A23.94 23.94 0 0 0 0 88v124a12 12 0 0 0 12 12h40a12 12 0 0 0 12-12v-84h84a12 12 0 0 0 12-12V76a12 12 0 0 0-12-12zm352 224h-40a12 12 0 0 0-12 12v84h-84a12 12 0 0 0-12 12v40a12 12 0 0 0 12 12h124a23.94 23.94 0 0 0 24-24V300a12 12 0 0 0-12-12z" /><Path d="M148 384H64v-84a12 12 0 0 0-12-12H12a12 12 0 0 0-12 12v124a23.94 23.94 0 0 0 24 24h124a12 12 0 0 0 12-12v-40a12 12 0 0 0-12-12zM488 64H364a12 12 0 0 0-12 12v40a12 12 0 0 0 12 12h84v84a12 12 0 0 0 12 12h40a12 12 0 0 0 12-12V88a23.94 23.94 0 0 0-24-24z" fill="#e84f3d" />
	</SVG> ),
	category: 'layout',
	keywords: [ __( 'Container', 'ms-gutenberg-blocks' ), 'mediasoep' ],
	supports: {
		align: [ 'wide', 'full' ],
	},
	attributes,
	edit,
	save,
	transforms,
	deprecated,
} );
