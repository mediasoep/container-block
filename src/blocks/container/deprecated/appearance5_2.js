/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { applyFilters } = wp.hooks;

const getAppearance5_2 = ( props ) => {
	const {
		attributes: {
			uniqueID,
			height,
			contentWidth,
			backgroundType,
			backgroundOpacity,
			backgroundColor,
			backgroundColor2,
			backgroundColorDirection = 0,
			backgroundColorType,
			backgroundImageUrl,
			backgroundFocalPoint,
			backgroundImageAttachment,
			backgroundImageParallax,
			isDark,
			topMargin,
			bottomMargin,
			topPadding,
			bottomPadding,
			leftPadding,
			rightPadding,
			tabletScalingFactor = 100,
			mobileScalingFactor = 50,
		},
	} = props;

	// Classes
	const wrapperClasses = classnames( 'ms-container--wrapper', {
		alignsmall: contentWidth === 'small',
		alignnormal: contentWidth === 'normal',
		alignwide: contentWidth === 'wide',
		alignfull: contentWidth === 'full',
	} );

	const hasBackground = backgroundColor || backgroundColorType === 'gradient' || backgroundType === 'image' || backgroundType === 'video';
	const opacityNumber = 1 * Math.round( backgroundOpacity / 1 );
	const backgroundClasses = classnames( {
		'has-background': hasBackground,
		'has-background-image': hasBackground && backgroundType === 'image',
		'has-background-video': hasBackground && backgroundType === 'video',
		[ `has-background-opacity-${ opacityNumber }` ]: hasBackground,
		'is-dark': isDark,
	} );

	const heightClass = classnames( {
		[ `has-height-${ height }` ]: height,
	} );

	const mergedClasses = classnames( backgroundClasses, heightClass );
	const classes = mergedClasses;

	// Inline styles
	let inlineStyles = {
		...( backgroundImageUrl && { backgroundImage: `url(${ backgroundImageUrl })` } ),
		...( backgroundImageUrl && { backgroundPosition: ! backgroundImageAttachment && backgroundFocalPoint ? `${ backgroundFocalPoint.x * 100 }% ${ backgroundFocalPoint.y * 100 }%` : 'center center' } ),
		...( backgroundImageUrl && { backgroundSize: 'cover' } ),
		...( backgroundImageUrl && { backgroundRepeat: 'no-repeat' } ),
		...( ( backgroundImageAttachment || backgroundImageParallax ) && { backgroundAttachment: 'fixed' } ),
	};
	inlineStyles = inlineStyles !== undefined && Object.entries( inlineStyles ).length !== 0 && backgroundType === 'image' ? inlineStyles : null;

	// Internal styles (<style></style>)
	const hasGradient = backgroundColorType === 'gradient' ? `background-image: linear-gradient(${ backgroundColorDirection || 0 }deg, ${ backgroundColor || 'transparent' }, ${ backgroundColor2 || 'transparent' });\n\t` : '';
	const backgroundInternalStyles = `#ms-container-${ uniqueID }.has-background:before {\n\t${ hasGradient }background-color: ${
		backgroundColor || 'transparent'
	};\n}\n`;

	const spacingStyles = {
		...( topMargin !== 0 && { 'margin-top': topMargin } ),
		...( bottomMargin !== 0 && { 'margin-bottom': bottomMargin } ),
		...( topPadding !== 0 && { 'padding-top': topPadding } ),
		...( bottomPadding !== 0 && { 'padding-bottom': bottomPadding } ),
		...( leftPadding !== 0 && { 'padding-left': leftPadding } ),
		...( rightPadding !== 0 && { 'padding-right': rightPadding } ),
	};
	let spacingsInternalStyles = '';

	if (
		spacingStyles !== undefined &&
			Object.entries( spacingStyles ).length !== 0
	) {
		const keys = Object.keys( spacingStyles );
		const last = keys[ keys.length - 1 ];

		let mobile = `@media screen and (max-width: ${ applyFilters( 'ms-gutenberg-blocks.container.mobile-max-width', '39.9375em' ) }) {\n\t#ms-container-${ uniqueID } {\n`;
		let tablet = `@media screen and (min-width: ${ applyFilters( 'ms-gutenberg-blocks.container.tablet-min-width', '40em' ) }) and (max-width: ${ applyFilters( 'ms-gutenberg-blocks.container.tablet-max-width', '63.9375em' ) }) {\n\t#ms-container-${ uniqueID } {\n`;
		let desktop = `@media screen and (min-width: ${ applyFilters( 'ms-gutenberg-blocks.container.desktop-min-width', '64em' ) }) {\n\t#ms-container-${ uniqueID } {\n`;

		for ( const style in spacingStyles ) {
			mobile += `\t\t${ style }: ${
				spacingStyles[ style ] * ( mobileScalingFactor / 100 )
			}rem;`;
			tablet += `\t\t${ style }: ${
				spacingStyles[ style ] * ( tabletScalingFactor / 100 )
			}rem;`;
			desktop += `\t\t${ style }: ${ spacingStyles[ style ] }rem;`;
			if ( last !== spacingStyles ) {
				mobile += '\n';
				tablet += '\n';
				desktop += '\n';
			}
		}

		mobile += '\n\t}';
		tablet += '\n\t}';
		desktop += '\n\t}';

		// Media query end brace.
		mobile += '\n}';
		tablet += '\n}';
		desktop += '\n}';

		spacingsInternalStyles = `\n${ mobile }\n\n${ tablet }\n\n${ desktop }`;
	}

	const internalStyles = classnames( backgroundInternalStyles, spacingsInternalStyles ); // Using 'classnames' to correctly merge styles.

	return [ classes, wrapperClasses, inlineStyles, internalStyles ];
};

export default getAppearance5_2;
