/**
 * BLOCK: MS Gutenberg Blocks - Container
 */
import classnames from 'classnames';
import getAppearance5_2 from './deprecated/appearance5_2';
const { InnerBlocks } = wp.blockEditor;

const deprecatedAttributes5_2 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	// Size settings
	height: {
		type: 'string',
		default: 'normal',
	},
	contentWidth: {
		type: 'string',
		default: 'normal',
	},
	// Background settings
	backgroundType: {
		type: 'string',
		default: '',
	},
	backgroundOpacity: {
		type: 'number',
		default: 5,
	},
	// Background color
	backgroundColorType: {
		type: 'string',
		default: '',
	},
	backgroundColor: {
		type: 'string',
		default: '',
	},
	backgroundColor2: {
		type: 'string',
		default: '',
	},
	backgroundColorDirection: {
		type: 'number',
		default: 0,
	},
	// Background image
	backgroundImageID: {
		type: 'number',
	},
	backgroundImageUrl: {
		type: 'string',
	},
	backgroundImageHeight: {
		type: 'number',
	},
	backgroundImageWidth: {
		type: 'number',
	},
	backgroundImageColor: {
		type: 'string',
		default: '',
	},
	backgroundFocalPoint: {
		type: 'object',
		default: {
			x: 0.5,
			y: 0.5,
		},
	},
	backgroundImageAttachment: {
		type: 'boolean',
		default: false,
	},
	backgroundImageParallax: {
		type: 'boolean',
		default: false,
	},
	backgroundImageParallaxSpeed: {
		type: 'number',
		default: 2,
	},
	isDark: {
		type: 'boolean',
		default: false,
	},
	// Padding
	topPadding: {
		type: 'number',
		default: 0,
	},
	bottomPadding: {
		type: 'number',
		default: 0,
	},
	leftPadding: {
		type: 'number',
		default: 0,
	},
	rightPadding: {
		type: 'number',
		default: 0,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
};

const deprecatedSave5_2 = ( props ) => {
	const {
		attributes: {
			uniqueID,
			backgroundType,
			backgroundImageUrl,
			backgroundImageHeight,
			backgroundImageWidth,
			backgroundImageParallax,
			backgroundImageParallaxSpeed,
		},
	} = props;
	const [ classes, wrapperClasses, inlineStyles, internalStyles ] = getAppearance5_2( props );

	const parallax = backgroundImageParallax && backgroundImageUrl ?
		{
			'data-parallax': 'scroll',
			'data-image-src': backgroundImageUrl,
			'data-z-index': 1,
			'data-natural-width': backgroundImageWidth,
			'data-natural-height': backgroundImageHeight,
			'data-speed': backgroundImageParallaxSpeed * 0.1,
		} :
		null;

	return (
		<div
			key="container"
			id={ `ms-container-${ uniqueID }` }
			className={ classes }
			style={ inlineStyles }
			{ ...parallax }
		>
			{ backgroundType === 'video' && (
				<video
					className="ms-video-background"
					autoPlay
					muted
					loop
					src={ backgroundImageUrl }
				/>
			) }
			<div className={ wrapperClasses }>
				<InnerBlocks.Content />
			</div>
			{ internalStyles && (
				<style
					dangerouslySetInnerHTML={ {
						__html: internalStyles,
					} }
				/>
			) }
		</div>
	);
};

const deprecatedAttributes4_6 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	// Size settings
	height: {
		type: 'string',
		default: 'normal',
	},
	contentWidth: {
		type: 'string',
		default: 'normal',
	},
	// Background settings
	backgroundType: {
		type: 'string',
		default: '',
	},
	backgroundOpacity: {
		type: 'number',
		default: 5,
	},
	// Background color
	backgroundColorType: {
		type: 'string',
		default: '',
	},
	backgroundColor: {
		type: 'string',
		default: '',
	},
	backgroundColor2: {
		type: 'string',
		default: '',
	},
	backgroundColorDirection: {
		type: 'number',
		default: 0,
	},
	// Background image
	backgroundImageID: {
		type: 'number',
	},
	backgroundImageUrl: {
		type: 'string',
	},
	backgroundImageHeight: {
		type: 'number',
	},
	backgroundImageWidth: {
		type: 'number',
	},
	backgroundImageColor: {
		type: 'string',
		default: '',
	},
	backgroundFocalPoint: {
		type: 'object',
		default: {
			x: 0.5,
			y: 0.5,
		},
	},
	backgroundImageAttachment: {
		type: 'boolean',
		default: false,
	},
	backgroundImageParallax: {
		type: 'boolean',
		default: false,
	},
	backgroundImageParallaxSpeed: {
		type: 'number',
		default: 2,
	},
	// Padding
	topPadding: {
		type: 'number',
		default: 0,
	},
	bottomPadding: {
		type: 'number',
		default: 0,
	},
	leftPadding: {
		type: 'number',
		default: 0,
	},
	rightPadding: {
		type: 'number',
		default: 0,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
	styles: {
		type: 'string',
		default: '',
	},
};

const deprecatedSave4_6 = props => {
	const {
		attributes: {
			uniqueID,
			height,
			contentWidth,
			backgroundType,
			backgroundOpacity,
			backgroundColorType,
			backgroundImageUrl,
			backgroundImageHeight,
			backgroundImageWidth,
			backgroundFocalPoint,
			backgroundImageAttachment,
			backgroundImageParallax,
			backgroundImageParallaxSpeed,
			styles,
		},
	} = props;

	const wrapperClasses = classnames( 'ms-container--wrapper', {
		alignsmall: contentWidth === 'small',
		alignnormal: contentWidth === 'normal',
		alignwide: contentWidth === 'wide',
		alignfull: contentWidth === 'full',
	} );

	const classes = classnames(
		'ms-container--background',
		`ms-container--background-opacity-${ 1 *
			Math.round( backgroundOpacity / 1 ) }`,
		{
			'ms-container--background':
				backgroundColorType === 'gradient' ||
				backgroundType === 'image' ||
				backgroundType === 'video',
			'ms-container--background-image': backgroundType === 'image',
			'ms-container--background-video': backgroundType === 'video',
			[ `ms-container--height-${ height }` ]: height,
		}
	);

	const backgroundImageStyles = {
		background: backgroundImageUrl ?
			`url(${ backgroundImageUrl }) no-repeat ${
				! backgroundImageAttachment && backgroundFocalPoint ?
					`${ backgroundFocalPoint.x * 100 }% ${ backgroundFocalPoint.y *
								100 }%` :
					'center center'
			} / cover` :
			undefined,
		backgroundAttachment: backgroundImageAttachment ? 'fixed' : undefined,
	};

	const inlineStyles =
		backgroundType === 'image' ? backgroundImageStyles : null;

	const parallax =
		backgroundImageParallax && backgroundImageUrl ?
			{
				'data-parallax': 'scroll',
				'data-image-src': backgroundImageUrl,
				'data-z-index': 1,
				'data-natural-width': backgroundImageWidth,
				'data-natural-height': backgroundImageHeight,
				'data-speed': backgroundImageParallaxSpeed * 0.1,
			} :
			null;

	return (
		<div
			key="container"
			id={ `ms-container-${ uniqueID }` }
			className={ classes }
			style={ inlineStyles }
			{ ...parallax }
		>
			{ backgroundType === 'video' && (
				<video
					className="ms-video-background"
					autoPlay
					muted
					loop
					src={ backgroundImageUrl }
				/>
			) }
			<div className={ wrapperClasses }>
				<InnerBlocks.Content />
			</div>
			{ styles && (
				<style
					dangerouslySetInnerHTML={ {
						__html: styles,
					} }
				/>
			) }
		</div>
	);
};

const deprecatedAttributes4_2 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	// Size settings
	height: {
		type: 'string',
		default: 'normal',
	},
	contentWidth: {
		type: 'string',
		default: 'normal',
	},
	// Background settings
	backgroundType: {
		type: 'string',
		default: '',
	},
	backgroundOpacity: {
		type: 'number',
		default: 5,
	},
	// Background color
	backgroundColorType: {
		type: 'string',
		default: '',
	},
	backgroundColor: {
		type: 'string',
		default: '',
	},
	backgroundColor2: {
		type: 'string',
		default: '',
	},
	backgroundColorDirection: {
		type: 'number',
		default: 0,
	},
	// Background image
	backgroundImageID: {
		type: 'number',
	},
	backgroundImageUrl: {
		type: 'string',
	},
	backgroundImageColor: {
		type: 'string',
		default: '',
	},
	backgroundFocalPoint: {
		type: 'object',
		default: {
			x: 0.5,
			y: 0.5,
		},
	},
	backgroundImageAttachment: {
		type: 'boolean',
		default: false,
	},
	// Padding
	topPadding: {
		type: 'number',
		default: 0,
	},
	bottomPadding: {
		type: 'number',
		default: 0,
	},
	leftPadding: {
		type: 'number',
		default: 0,
	},
	rightPadding: {
		type: 'number',
		default: 0,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
	styles: {
		type: 'string',
		default: '',
	},
};

const deprecatedSave4_2 = props => {
	const {
		attributes: {
			uniqueID,
			height,
			contentWidth,
			backgroundType,
			backgroundOpacity,
			backgroundColorType,
			backgroundImageUrl,
			backgroundFocalPoint,
			backgroundImageAttachment,
			styles,
		},
	} = props;

	const wrapperClasses = classnames( 'ms-container--wrapper', {
		alignsmall: contentWidth === 'small',
		alignnormal: contentWidth === 'normal',
		alignwide: contentWidth === 'wide',
		alignfull: contentWidth === 'full',
	} );

	const classes = classnames(
		'ms-container--background',
		`ms-container--background-opacity-${ 1 *
			Math.round( backgroundOpacity / 1 ) }`,
		{
			'ms-container--background':
				backgroundColorType === 'gradient' ||
				backgroundType === 'image' ||
				backgroundType === 'video',
			'ms-container--background-image': backgroundType === 'image',
			'ms-container--background-video': backgroundType === 'video',
			[ `ms-container--height-${ height }` ]: height,
		}
	);

	const backgroundImageStyles = {
		background: backgroundImageUrl ?
			`url(${ backgroundImageUrl }) no-repeat ${
				! backgroundImageAttachment && backgroundFocalPoint ?
					`${ backgroundFocalPoint.x * 100 }% ${ backgroundFocalPoint.y *
						100 }%` :
					'center center'
			} / cover` :
			undefined,
		backgroundAttachment: backgroundImageAttachment ? 'fixed' : undefined,
	};

	const inlineStyles = backgroundType === 'image' ? backgroundImageStyles : null;

	return (
		<div
			key="container"
			id={ `ms-container-${ uniqueID }` }
			className={ classes }
			style={ inlineStyles }
		>
			{ backgroundType === 'video' && (
				<video
					className="ms-video-background"
					autoPlay
					muted
					loop
					src={ backgroundImageUrl }
				/>
			) }
			<div className={ wrapperClasses }>
				<InnerBlocks.Content />
			</div>
			{ styles &&
				<style
					dangerouslySetInnerHTML={ {
						__html: styles,
					} }
				/>
			}
		</div>
	);
};

const deprecatedAttributes3_0 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	// Size settings
	height: {
		type: 'string',
		default: 'normal',
	},
	contentWidth: {
		type: 'string',
		default: 'normal',
	},
	// Background settings
	backgroundType: {
		type: 'string',
		default: '',
	},
	backgroundOpacity: {
		type: 'number',
		default: 5,
	},
	// Background color
	backgroundColorType: {
		type: 'string',
		default: '',
	},
	backgroundColor: {
		type: 'string',
		default: '',
	},
	backgroundColor2: {
		type: 'string',
		default: '',
	},
	backgroundColorDirection: {
		type: 'number',
		default: 0,
	},
	// Background image
	backgroundImageID: {
		type: 'number',
	},
	backgroundImageUrl: {
		type: 'string',
	},
	backgroundImageColor: {
		type: 'string',
		default: '',
	},
	backgroundFocalPoint: {
		type: 'object',
		default: {
			x: 0.5,
			y: 0.5,
		},
	},
	backgroundImageAttachment: {
		type: 'boolean',
		default: false,
	},
	// Padding
	topPadding: {
		type: 'number',
		default: 0,
	},
	bottomPadding: {
		type: 'number',
		default: 0,
	},
	leftPadding: {
		type: 'number',
		default: 0,
	},
	rightPadding: {
		type: 'number',
		default: 0,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
};

const deprecatedSave3_0 = props => {
	const {
		attributes: {
			uniqueID,
			height,
			contentWidth,
			backgroundType,
			backgroundOpacity,
			backgroundColor,
			backgroundColor2,
			backgroundColorDirection = 0,
			backgroundColorType,
			backgroundImageUrl,
			backgroundFocalPoint,
			backgroundImageAttachment,
			topMargin,
			bottomMargin,
			topPadding,
			bottomPadding,
			leftPadding,
			rightPadding,
		},
	} = props;

	const classes = classnames(
		'ms-container--background',
		`ms-container--background-opacity-${ 1 *
				Math.round( backgroundOpacity / 1 ) }`,
		{
			'ms-container--background':
					backgroundColorType === 'gradient' ||
					backgroundType === 'image' ||
					backgroundType === 'video',
			'ms-container--background-image': backgroundType === 'image',
			'ms-container--background-video': backgroundType === 'video',
			[ `ms-container--height-${ height }` ]: height,
		}
	);

	const backgroundImageStyles = {
		background: backgroundImageUrl ?
			`url(${ backgroundImageUrl }) no-repeat ${
				! backgroundImageAttachment && backgroundFocalPoint ?
					`${ backgroundFocalPoint.x * 100 }% ${ backgroundFocalPoint.y *
									100 }%` :
					'center center'
			} / cover` :
			undefined,
		backgroundAttachment: backgroundImageAttachment ? 'fixed' : undefined,
	};

	let styles = {
		marginTop: topMargin !== 0 ? topMargin + 'rem' : undefined,
		marginBottom: bottomMargin !== 0 ? bottomMargin + 'rem' : undefined,
		paddingTop: topPadding !== 0 ? topPadding + 'rem' : undefined,
		paddingBottom: bottomPadding !== 0 ? bottomPadding + 'rem' : undefined,
		paddingLeft: leftPadding !== 0 ? leftPadding + 'rem' : undefined,
		paddingRight: rightPadding !== 0 ? rightPadding + 'rem' : undefined,
	};

	styles =
			backgroundType === 'image' ?
				{ ...backgroundImageStyles, ...styles } :
				styles;

	const wrapperClasses = classnames( 'ms-container--wrapper', {
		alignsmall: contentWidth === 'small',
		alignnormal: contentWidth === 'normal',
		alignwide: contentWidth === 'wide',
		alignfull: contentWidth === 'full',
	} );

	const hasGradient =
			backgroundColorType === 'gradient' ?
				`background-image: linear-gradient(${ backgroundColorDirection ||
						0 }deg, ${ backgroundColor || 'transparent' }, ${ backgroundColor2 ||
						'transparent' });` :
				'';

	const inlineStyles = `
			#ms-container-${ uniqueID }.ms-container--background:before {
				${ hasGradient }
				background-color: ${ backgroundColor || 'transparent' };
			}`;

	return (
		<div
			key="container"
			id={ `ms-container-${ uniqueID }` }
			className={ classes }
			style={ styles }
		>
			{ backgroundType === 'video' && (
				<video
					className="ms-video-background"
					autoPlay
					muted
					loop
					src={ backgroundImageUrl }
				/>
			) }
			<div className={ wrapperClasses }>
				<InnerBlocks.Content />
			</div>
			<style
				dangerouslySetInnerHTML={ {
					__html: inlineStyles,
				} }
			/>
		</div>
	);
};

const deprecatedAttributes2_0 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	align: {
		type: 'string',
		default: 'full',
	},
	// Size settings
	height: {
		type: 'string',
		default: 'normal',
	},
	contentWidth: {
		type: 'string',
		default: 'normal',
	},
	// Background settings
	backgroundType: {
		type: 'string',
		default: 'none',
	},
	// Background color
	backgroundColor: {
		type: 'string',
		default: '',
	},
	backgroundColorOpacity: {
		type: 'number',
		default: 10,
	},
	// Background image
	backgroundImageID: {
		type: 'string',
		default: '',
	},
	backgroundImageUrl: {
		type: 'string',
		default: '',
	},
	backgroundImageColor: {
		type: 'string',
		default: '',
	},
	backgroundImageOpacity: {
		type: 'number',
		default: 5,
	},
	backgroundImageAttachment: {
		type: 'boolean',
		default: false,
	},
	// backgroundImagePosition: {
	// 	type: 'string',
	// 	default: 'center center',
	// },
	// backgroundImageRepeat: {
	// 	type: 'string',
	// 	default: 'no-repeat',
	// },
	// backgroundImageSize: {
	// 	type: 'string',
	// 	default: 'cover',
	// },
	// Padding
	topPadding: {
		type: 'number',
		default: 0,
	},
	bottomPadding: {
		type: 'number',
		default: 0,
	},
	leftPadding: {
		type: 'number',
		default: 0,
	},
	rightPadding: {
		type: 'number',
		default: 0,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
};

const deprecatedSave2_0 = props => {
	const {
		attributes: {
			uniqueID,
			height,
			contentWidth,
			backgroundColor,
			backgroundColorOpacity,
			backgroundType,
			backgroundImageUrl,
			backgroundImageColor,
			backgroundImageOpacity,
			backgroundImageAttachment,
			topMargin,
			bottomMargin,
			topPadding,
			bottomPadding,
			leftPadding,
			rightPadding,
		},
	} = props;

	const classes = classnames( {
		[ `ms-container--background-color-opacity-${ 1 *
			Math.round( backgroundColorOpacity / 1 ) }` ]: backgroundType === 'color',
		[ `ms-container--background-image-opacity-${ 1 *
			Math.round( backgroundImageOpacity / 1 ) }` ]: backgroundType === 'image',
		'ms-container--background':
			backgroundType === 'color' && backgroundType === 'image',
		'ms-container--background-color': backgroundType === 'color',
		'ms-container--background-image': backgroundType === 'image',
		[ `ms-container--height-${ height }` ]: height,
	} );

	const backgroundColorStyles = {
		'--ms-background-color': backgroundColor,
	};

	const backgroundImageStyles = {
		backgroundImage: backgroundImageUrl ?
			`url(${ backgroundImageUrl })` :
			undefined,
		backgroundAttachment: backgroundImageAttachment ? 'fixed' : undefined,
		'--ms-background-image-color': backgroundImageColor,
	};

	let styles = {
		marginTop: topMargin !== 0 ? topMargin + 'rem' : undefined,
		marginBottom: bottomMargin !== 0 ? bottomMargin + 'rem' : undefined,
		paddingTop: topPadding !== 0 ? topPadding + 'rem' : undefined,
		paddingBottom: bottomPadding !== 0 ? bottomPadding + 'rem' : undefined,
		paddingLeft: leftPadding !== 0 ? leftPadding + 'rem' : undefined,
		paddingRight: rightPadding !== 0 ? rightPadding + 'rem' : undefined,
	};
	styles =
		backgroundType === 'color' ?
			{ ...backgroundColorStyles, ...styles } :
			styles;
	styles =
		backgroundType === 'image' ?
			{ ...backgroundImageStyles, ...styles } :
			styles;

	const wrapperClasses = classnames( 'ms-container--wrapper', {
		alignnormal: contentWidth === 'normal',
		alignwide: contentWidth === 'wide',
		alignfull: contentWidth === 'full',
	} );

	return (
		<div
			id={ `ms-container-${ uniqueID }` }
			className={ `${ classes }` }
			style={ styles }
		>
			<div className={ wrapperClasses }>
				<InnerBlocks.Content />
			</div>
			<style
				dangerouslySetInnerHTML={ {
					__html: `
                        #ms-container-${ uniqueID }.ms-container--background-color:before {
                            background-color: ${ backgroundColor || '#fefefe' };
                        }

                        #ms-container-${ uniqueID }.ms-container--background-image:before {
                            background-color: ${ backgroundImageColor ||
															'#fefefe' };
                        }
                    `,
				} }
			/>
		</div>
	);
};

const deprecated = [
	{
		attributes: deprecatedAttributes5_2,
		save: deprecatedSave5_2,
	},
	{
		attributes: deprecatedAttributes4_6,
		save: deprecatedSave4_6,
	},
	{
		attributes: deprecatedAttributes4_2,
		save: deprecatedSave4_2,
	},
	{
		attributes: deprecatedAttributes3_0,
		save: deprecatedSave3_0,
	},
	{
		attributes: deprecatedAttributes2_0,
		save: deprecatedSave2_0,
	},
];

export default deprecated;
