/**
 * BLOCK: MS Gutenberg Blocks - Panel
 */

/**
 * External dependencies
 */
import classnames from 'classnames';

/**
 * WordPress dependencies
 */
const { InnerBlocks, RichText } = wp.blockEditor;

const deprecatedAttributes4_6_1 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	activeControl: {
		type: 'string',
	},
	activePanel: {
		type: 'number',
		default: 0,
	},
	timestamp: {
		type: 'number',
		default: 0,
	},
	tabsTitle: {
		source: 'query',
		selector: '.tabs-title',
		query: {
			content: {
				type: 'array',
				source: 'children',
				selector: '.tabs-title-a',
			},
		},
		default: [
			{ content: 'Tab' },
		],
	},
	styles: {
		type: 'string',
		default: '',
	},
	// Responsive settings
	desktopType: {
		type: 'string',
		default: 'tabs',
	},
	tabletType: {
		type: 'string',
		default: 'tabs',
	},
	mobileType: {
		type: 'string',
		default: 'tabs',
	},
	// Accordion settings
	accordionInitiallyClosePanes: {
		type: 'boolean',
		default: false,
	},
	accordionAllowAllClosed: {
		type: 'boolean',
		default: false,
	},
	accordionMultiExpand: {
		type: 'boolean',
		default: false,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
};

const deprecatedSave4_6_1 = ( props ) => {
	const {
		className,
		attributes: {
			styles,
			uniqueID,
			activePanel,
			tabsTitle,
			desktopType,
			tabletType,
			mobileType,
			accordionInitiallyClosePanes,
			accordionAllowAllClosed,
			accordionMultiExpand,
		},
	} = props;

	const classes = classnames( 'ms-responsive-accordion-tabs', className );

	const responsiveAccordionTabs = classnames( {
		[ mobileType ]: mobileType,
		[ `medium-${ tabletType }` ]: tabletType,
		[ `large-${ desktopType }` ]: desktopType,
	} );

	const accordionOptions = [ desktopType, tabletType, mobileType ].some(
		el => el === 'accordion'
	) ?
		{
			'data-allow-all-closed': accordionAllowAllClosed,
			'data-multi-expand': accordionMultiExpand,
		} :
		{};

	return (
		<div id={ `block_${ uniqueID }` } className={ classes }>
			<ul
				id={ uniqueID }
				className="tabs"
				data-responsive-accordion-tabs={ responsiveAccordionTabs }
				{ ...accordionOptions }
			>
				{ tabsTitle.map( ( value, i ) => {
					return (
						<li
							key={ i }
							className={ classnames( 'tabs-title', {
								'is-active': activePanel === i && ! accordionInitiallyClosePanes,
							} ) }
						>
							<RichText.Content
								tagName="a"
								className="tabs-title-a"
								href={ `#panel${ i }` }
								value={ value.content }
							/>
						</li>
					);
				} ) }
			</ul>
			<div className="tabs-content" data-tabs-content={ uniqueID }>
				<InnerBlocks.Content />
			</div>
			{ styles &&
				<style
					dangerouslySetInnerHTML={ {
						__html: styles,
					} }
				/>
			}
		</div>
	);
};

const deprecatedAttributes4_2_2 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	activeControl: {
		type: 'string',
	},
	activePanel: {
		type: 'number',
		default: 0,
	},
	timestamp: {
		type: 'number',
		default: 0,
	},
	tabsTitle: {
		source: 'query',
		selector: '.tabs-title',
		query: {
			content: {
				type: 'array',
				source: 'children',
				selector: '.tabs-title-a',
			},
		},
	},
	styles: {
		type: 'string',
		default: '',
	},
	// Responsive settings
	desktopType: {
		type: 'string',
		default: 'tabs',
	},
	tabletType: {
		type: 'string',
		default: 'tabs',
	},
	mobileType: {
		type: 'string',
		default: 'tabs',
	},
	// Accordion settings
	accordionInitiallyClosePanes: {
		type: 'boolean',
		default: false,
	},
	accordionAllowAllClosed: {
		type: 'boolean',
		default: false,
	},
	accordionMultiExpand: {
		type: 'boolean',
		default: false,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
};

const deprecatedAttributes4_0 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	activeControl: {
		type: 'string',
	},
	activePanel: {
		type: 'number',
		default: 0,
	},
	timestamp: {
		type: 'number',
		default: 0,
	},
	tabsTitle: {
		source: 'query',
		selector: '.tabs-title',
		query: {
			content: {
				type: 'array',
				source: 'children',
				selector: '.tabs-title-a',
			},
		},
	},
	styles: {
		type: 'string',
		default: '',
	},
	// Responsive settings
	desktopType: {
		type: 'string',
		default: 'tabs',
	},
	tabletType: {
		type: 'string',
		default: 'tabs',
	},
	mobileType: {
		type: 'string',
		default: 'tabs',
	},
	// Accordion settings
	accordionInitiallyClosePanes: {
		type: 'boolean',
		default: false,
	},
	accordionAllowAllClosed: {
		type: 'boolean',
		default: false,
	},
	accordionMultiExpand: {
		type: 'boolean',
		default: false,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
};

const deprecatedSave4_0 = props => {
	const {
		styles,
		uniqueID,
		activePanel,
		tabsTitle,
		desktopType,
		tabletType,
		mobileType,
		accordionInitiallyClosePanes,
		accordionAllowAllClosed,
		accordionMultiExpand,
	} = attributes;

	const classes = classnames( 'ms-responsive-accordion-tabs', className );

	const responsiveAccordionTabs = classnames( {
		[ mobileType ]: mobileType,
		[ `medium-${ tabletType }` ]: tabletType,
		[ `large-${ desktopType }` ]: desktopType,
	} );

	const accordionOptions = [ desktopType, tabletType, mobileType ].some(
		el => el === 'accordion'
	) ?
		{
			'data-allow-all-closed': accordionAllowAllClosed,
			'data-multi-expand': accordionMultiExpand,
		} :
		{};

	return (
		<div id={ `block_${ uniqueID }` } className={ classes }>
			<ul
				id={ uniqueID }
				className="tabs"
				data-responsive-accordion-tabs={ responsiveAccordionTabs }
				{ ...accordionOptions }
			>
				{ tabsTitle.map( ( value, i ) => {
					return (
						<li
							key={ i }
							className={ classnames( 'tabs-title', {
								'is-active': activePanel === i && ! accordionInitiallyClosePanes,
							} ) }
						>
							<RichText.Content
								tagName="a"
								className="tabs-title-a"
								href={ `#panel${ i }` }
								value={ value.content }
							/>
						</li>
					);
				} ) }
			</ul>
			<div className="tabs-content" data-tabs-content={ uniqueID }>
				<InnerBlocks.Content />
			</div>
			{ styles &&
				<style
					dangerouslySetInnerHTML={ {
						__html: styles,
					} }
				/>
			}
		</div>
	);
};

const deprecatedAttributes3_0 = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	activeControl: {
		type: 'string',
	},
	activePanel: {
		type: 'number',
		default: 0,
	},
	timestamp: {
		type: 'number',
		default: 0,
	},
	tabsTitle: {
		source: 'query',
		selector: '.tabs-title',
		query: {
			content: {
				type: 'array',
				source: 'children',
				selector: '.tabs-title-a',
			},
		},
	},
	// Responsive settings
	desktopType: {
		type: 'string',
		default: 'tabs',
	},
	tabletType: {
		type: 'string',
		default: 'tabs',
	},
	mobileType: {
		type: 'string',
		default: 'tabs',
	},
	// Accordion settings
	accordionAllowAllClosed: {
		type: 'boolean',
		default: false,
	},
	accordionMultiExpand: {
		type: 'boolean',
		default: false,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
};

const deprecatedSave3_0 = props => {
	const {
		className,
		attributes: {
			activePanel,
			uniqueID,
			tabsTitle,
			topMargin,
			bottomMargin,
			mobileType,
			tabletType,
			desktopType,
			accordionAllowAllClosed,
			accordionMultiExpand,
		},
	} = props;

	const classes = classnames( 'ms-tabs', className );
	const styles = {
		marginTop: topMargin !== 0 ? topMargin + 'rem' : undefined,
		marginBottom: bottomMargin !== 0 ? bottomMargin + 'rem' : undefined,
	};
	const responsiveAccordionTabs = classnames( {
		[ mobileType ]: mobileType,
		[ `medium-${ tabletType }` ]: tabletType,
		[ `large-${ desktopType }` ]: desktopType,
	} );
	const accordionOptions = [ desktopType, tabletType, mobileType ].some(
		el => el === 'accordion'
	) ?
		{
			'data-allow-all-closed': accordionAllowAllClosed,
			'data-multi-expand': accordionMultiExpand,
		} :
		{};

	return (
		<div id={ `ms-tabs-${ uniqueID }` } className={ classes } style={ styles }>
			<ul
				id={ uniqueID }
				className="tabs"
				data-responsive-accordion-tabs={ responsiveAccordionTabs }
				{ ...accordionOptions }
			>
				{ tabsTitle.map( ( value, i ) => {
					return (
						<li
							key={ i }
							className={ classnames( 'tabs-title', {
								'is-active': activePanel === i,
							} ) }
						>
							<RichText.Content
								tagName="a"
								className="tabs-title-a"
								href={ `#panel${ i }` }
								value={ value.content }
							/>
						</li>
					);
				} ) }
			</ul>
			<div className="tabs-content" data-tabs-content={ uniqueID }>
				<InnerBlocks.Content />
			</div>
		</div>
	);
};

const deprecated = [
	{
		attributes: deprecatedAttributes4_6_1,
		save: deprecatedSave4_6_1,
	},
	{
		attributes: deprecatedAttributes4_2_2,
	},
	{
		attributes: deprecatedAttributes4_0,
		save: deprecatedSave4_0,
	},
	{
		attributes: deprecatedAttributes3_0,
		save: deprecatedSave3_0,
	},
];

export default deprecated;
