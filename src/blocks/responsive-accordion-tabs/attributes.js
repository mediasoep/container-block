/**
 * BLOCK: MS Gutenberg Blocks - Responsive Accordion Tabs
 */
const attributes = {
	uniqueID: {
		type: 'string',
		default: '',
	},
	activeControl: {
		type: 'string',
	},
	activePanel: {
		type: 'number',
		default: 0,
	},
	timestamp: {
		type: 'number',
		default: 0,
	},
	tabsTitle: {
		source: 'query',
		selector: '.tabs-title',
		query: {
			content: {
				type: 'array',
				source: 'children',
				selector: '.tabs-title-a',
			},
		},
		default: [
			{ content: 'Tab' },
		],
	},
	styles: {
		type: 'string',
		default: '',
	},
	// Responsive settings
	desktopType: {
		type: 'string',
		default: 'tabs',
	},
	tabletType: {
		type: 'string',
		default: 'tabs',
	},
	mobileType: {
		type: 'string',
		default: 'tabs',
	},
	// Accordion settings
	accordionInitiallyClosePanes: {
		type: 'boolean',
		default: false,
	},
	accordionAllowAllClosed: {
		type: 'boolean',
		default: false,
	},
	accordionMultiExpand: {
		type: 'boolean',
		default: false,
	},
	// Margin
	topMargin: {
		type: 'number',
		default: 0,
	},
	bottomMargin: {
		type: 'number',
		default: 0,
	},
	// Tablet and mobile scaling factor
	tabletScalingFactor: {
		type: 'number',
		default: 100,
	},
	mobileScalingFactor: {
		type: 'number',
		default: 50,
	},
};

export default attributes;
