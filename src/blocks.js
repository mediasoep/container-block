/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

import './options/conditional-logic/attributes.js';
import './blocks/anchor/index.js';
import './blocks/animate/index.js';
import './blocks/container/index.js';
import './blocks/layouts/index.js';
import './blocks/responsive-accordion-tabs/index.js';
import './blocks/panel/index.js';
import './formats/index.js';
